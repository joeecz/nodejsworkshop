const db = require("./db");

exports.saveUser = (user) => {
    const result = db.set(user.id, user);
    return result;
}

exports.loadUser = (id) => {
    return db.get(id);
}