const database = {}

exports.set = (key, obj) => {
    database[key] = obj;
    return true;
}

exports.get = (key) => {
    return database[key];
}