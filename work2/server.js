const ctrl = require("./controller");

if (!ctrl.saveUser({id:"user1", name:"Joe"})) {
    console.log("error saving user");
} else {
    let user = ctrl.loadUser("user1");
    console.log(user);
}